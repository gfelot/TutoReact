import React, { Component } from "react";
import Home from "./pages/Home";
import Header from "./components/Header";
import About from "./pages/About";
import Post from "./pages/Post";

// Pour react-router-redux
import { history } from "./store";
import { Route } from 'react-router'
import { ConnectedRouter as Router} from 'react-router-redux'

// CSS Bulma
import "bulma/css/bulma.css";

class App extends Component {
  render() {
    return (
      <Router history={history} >
        <div>
          <Header />
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/post/:id" component={Post} />
        </div>
      </Router>
    )
  }
}

export default App;
