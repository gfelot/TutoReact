import React from 'react';

const Post = ({ match,location }) => {
  return (
    <div>
      <section className="hero is-danger is-fullheight">
        <div className="hero-body">
          <div className="container">

            <h1 className="title">
              Page View
            </h1>

            <h2 className="subtitle">
              ID : #{match.params.id}

            </h2>

            <br />
            
            <h2 className="subtitle">
              Vous êtes sur la page : {location.pathname}
            </h2>

          </div>
        </div>
      </section>
    </div>
  );
};

export default Post;
