import React from 'react';
import GifContainer from "../components/GifContainer";
import SearchBar from "../components/SearchBar";

const Home = () => {
	return (
		<React.Fragment>
			<SearchBar />
			<GifContainer />
		</React.Fragment>
	)
}

export default Home;