export const actions = {
  FETCH_LOADING: "FETCH_GIPHY::DATA_LOADING",
  FETCH_ERROR: "FETCH_GIPHY::DATA_ERROR",
  FETCH_SUCCESS: "FETCH_GIPHY::DATA"
}

const fetchGifLoading = () => {
  return {
    type: actions.FETCH_LOADING,
    payload: {
      loading: true,
      hasError: false,
      data: {}
    }
  };
};

const fetchGifSuccess = data => {
  console.log('Yolo');
  
  return {
    type: actions.FETCH_SUCCESS,
    payload: {
      loading: false,
      hasError: false,
      data
    }
  };
};

const fetchGifError = () => {
  return {
    type: actions.FETCH_ERROR,
    payload: {
      loading: false,
      hasError: true,
      data: {}
    }
  };
};

//Action Creators
export const fetchGif = tags => {
  return async dispatch => {
    dispatch(fetchGifLoading())

    const BASE_URL = 'http://api.giphy.com/v1/gifs/random?'
    const API_KEY = 'api_key=oHvrulSMjSSOJslU6m76msCmBf7LjELV'
    const TAGS = `&tag=${tags}`
    const END_POINT = `${BASE_URL}${API_KEY}${TAGS}`

    try {
      const response = await fetch(END_POINT)
      const json = await response.json()
      console.log('JSON :', json);
      dispatch(fetchGifSuccess(json.data))
    } catch (error) {
      console.log('Error', error)
      dispatch(fetchGifError(true))
    }
  }
}
