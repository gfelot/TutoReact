import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers/reducers";

// React Router + Redux link
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

export const history = createHistory()

const enhancers = [];
const middleware = [
  thunk,
  routerMiddleware(history)
]

// Pour l'outils dans le navigateur Redux-devtools
if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const Store = createStore(rootReducer, composedEnhancers);

export default Store;
