import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchGif } from "../actions/fetchGif";

class SearchBar extends Component {

	state = {
		tag: ''
	}

	handleSearchBarClick = () => {
		const { tag } = this.state
		if (tag === '') {
			this.props.fetchGif('dogs');
		} else {
			this.props.fetchGif(tag);
		}
	}

	handleInputChange = e => {
		this.setState({ tag: e.target.value });
	}

	handleKeyPress = e => {
		if (e.key === 'Enter') {
      const { tag } = this.state;
      if (tag === '') {
        this.props.fetchGif('dog');
      } else {
        this.props.fetchGif(this.state.tag);
      }
    }
	}

  render() {
    return (
      <section className="section">
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <div className="field has-addons">
              <div className="control is-expanded">
                <input className="input" type="text" placeholder="dog" onChange={this.handleInputChange} onKeyPress={this.handleKeyPress} />
              </div>
              <div className="control">
                <a className="button is-info" onClick={this.handleSearchBarClick} >Random!</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default connect(null, { fetchGif })(SearchBar);