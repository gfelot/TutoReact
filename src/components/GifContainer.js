import React, { Component } from "react";
import GifCard from "./GifCard";
import GifCardError from "./GifCardError";
import GifCardloading from "./GifCardloading";
import { fetchGif } from "../actions/fetchGif";

import { connect } from 'react-redux';

class GifContainer extends Component {

  async componentDidMount() {
    this.props.fetchGif(this.props.tags);
  }

  // Conditional rendering
  render() {
    if (this.props.loading) { 
      return <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCardloading />
          </div>
        </div>;
    } else if (this.props.hasError) { 
      return (
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCardError />
          </div>
        </div>
      );
    } else {
      return <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCard data={ this.props.data } />
          </div>
        </div>;
    }
  }
}

function mapStateToProps(state) {
  return {
    ...state.giphy
  };
}

export default connect(mapStateToProps, { fetchGif })(GifContainer); // Modifier l'existant