import { combineReducers } from "redux";
import GiphyReducer from "./giphyReducers";

// Routing with Redux
import { routerReducer as routing } from "react-router-redux";


const rootReducer = combineReducers({
  giphy: GiphyReducer,
  routing,
});

export default rootReducer;
