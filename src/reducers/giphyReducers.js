import { actions } from '../actions/fetchGif'

const initialState = {
  loading: true,
  hasError: false,
  tags: "dog",
  data: {}
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	const newState = {...state, ...payload};

  switch (type) {
    case actions.FETCH_ERROR:
    case actions.FETCH_LOADING:
    case actions.FETCH_SUCCESS:
      return newState;

    default:
      return state;
  }
};
